import { KeyValuePipe } from '@angular/common';
import { Directive, OnInit, Input, ElementRef } from '@angular/core';
import { CoreService } from '../services/core.service';

@Directive({
  selector: '[appValidateSpetialCharacters]'
})
export class ValidateSpetialCharactersDirective {

  constructor(public el: ElementRef) {
    //this.el.nativeElement.classList.add('uk-hidden');
  }

  ngOnInit() {
     
    this.el.nativeElement.addEventListener("keyup", (event:any) => {
       
       event.target.value = this.deleteCharacters(event.target.value)
    })
  }

  deleteCharacters(string:any){
    
    string = string.replace(/[^a-zA-Z0-9\s+]/g, '')
    return string
  }
}
 