import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {RankingService } from '../ranking.service';
import Swal from 'sweetalert2';
import html2canvas from 'html2canvas';
@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class PreviewComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: RankingService ) { }

  eventId:any = '0'
  event : any = {}
  all : any = {}
  top3 : any = {}
  categories : any = {}
  grantcat : any = {}

  @ViewChild("tablaparacaptura") tablaparacaptura : any;
  @ViewChild("resultPageTable") resultPageTable : any;

  ngOnInit(): void {
    this.eventId = this.route.snapshot.paramMap.get('event')
    this.getGenetalranking()
    
  }

  getGenetalranking(){
    this.service.getRanking(this.eventId).subscribe((response:any) => {
      console.log(response);
      this.event = response.event;
      this.all = response.all;
      this.top3 = response.top3;
      this.categories = response.categories;
      this.grantcat = response.grantcat;
      

    });
  }

  generateHtmlCanva(idhtml: string){
    //let container : HTMLElement = this.resultPageTable.nativeElement;
    let htmlP : HTMLElement = document.querySelector('#'+idhtml) || document.createElement('div');
    html2canvas(htmlP).then((canva:any)=> {
      let contenido : any = `
        <!doctype html>
        <html>
          <head>
            <title>REPORT TO PRINT</title>
            <style>
              *{
                margin: 0;
                padding: 0;
                color: #000;
                background-color: white;
              ]
            </style>
          </head>
          <body>
            <img src="${canva.toDataURL()}" />
          </body>
        </html>
      `;
      //container.appendChild(canva);
      //////////////////////////////
      let printWin : any = window.open('','','width=700,height=500');
      printWin.document.open();
      printWin.document.write(contenido);
      printWin.document.close();
      printWin.focus();
      printWin.print();
    });
  }
 
}
