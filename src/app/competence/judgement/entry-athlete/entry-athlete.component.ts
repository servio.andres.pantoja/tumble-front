import { Component, OnInit, Input, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { JudgementService } from '../judgement.service';
import Swal from 'sweetalert2';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-entry-athlete',
  templateUrl: './entry-athlete.component.html',
  styleUrls: ['./entry-athlete.component.scss']
})
export class EntryAthleteComponent implements OnInit {

  grandParameters: any = [];
  params: any = {};
  paramsforsend: any = {};
  aviableSend: any = false;
  total: any = 0;
  hitzero: any = false;
  showAceptButton: any = true;

  @Input() entryid: any = 0;
  @Input() changes: any = 0;
  @Input() firstMainStatus: any = 0;
  @Input() dataclaseentry: any = {};
  @Output() stateChange = new EventEmitter<any>();
  @Output() stateChangeBar = new EventEmitter<any>();
  @Output() claimObjet = new EventEmitter<any>();
  @Output() eventFinishCreate = new EventEmitter<any>();
  @Output() launchMainJudgeStatus = new EventEmitter<any>();


  @ViewChild("tablaparacaptura") tablaparacaptura: any;
  @ViewChild("resultPageTable") resultPageTable: any;

  constructor(private service: JudgementService, private myElement: ElementRef) { }

  ngOnInit(): void {
    this.getParamsAndCriterias();
    this.service.getGeneralJudgmentParams(this.entryid).subscribe((response: any) => {
      this.paramsforsend = response;

      this.service.getPenaltyJudgmentParams(this.entryid).subscribe((response: any) => {
        this.paramsforsend.penaltyTotal = response.penaltyTotal;
        let total = this.paramsforsend.generalTotal - this.paramsforsend.penaltyTotal;
        this.total = (total > 0) ? total : 0;
      });
    });

  }

  generateHtmlCanva(table: Number) {
    let container: HTMLElement = this.resultPageTable.nativeElement;
    let contentPrint: HTMLElement;
    if(table === 102){
      contentPrint = this.myElement.nativeElement.querySelector("#summary");
    }else if(table === 101){
      contentPrint = this.myElement.nativeElement.querySelector("#penalty");
    }else if(table === 100){
      contentPrint = this.tablaparacaptura.nativeElement;
    }else{
      contentPrint = this.myElement.nativeElement.querySelector("#tablacaptura" + table);
    }

    html2canvas(contentPrint).then((canva: any) => {
      let contenido: any = `
        <!doctype html>
        <html>
          <head>
            <title>REPORT TO PRINT</title>
          </head>
          <body>
            <img src="${canva.toDataURL()}" />
          </body>
        </html>
      `;
      container.appendChild(canva);
      //////////////////////////////
      let printWin: any = window.open('', '', 'width=700,height=500');
      printWin.document.open();
      printWin.document.write(contenido);
      printWin.document.close();
      printWin.focus();
      printWin.print();
    });
  }



  ngOnChanges() {

    if (this.changes == 1) {
      this.getParamsAndCriterias();
      this.stateChange.emit(0);
    }

  }

  getParamsAndCriterias() {
    this.service.getAtlete(this.entryid).subscribe((response: any) => {
      this.params = response;
      // Create a map to order and map information for print

      let mapGrandParameter = new Map();
      for (let index = 0; index < this.params.parameters.length; index++) {
        const parameter = this.params.parameters[index];
        if(!parameter.gp_id){
          parameter.gp_id = 99;
        }
        if(parameter.gp_id){
          if (!mapGrandParameter.get(parameter.gp_id)) {
            mapGrandParameter.set(parameter.gp_id, { gp_id: parameter.gp_id, gp_title: parameter.gp_title, parameters: [] });
          }
          mapGrandParameter.get(parameter.gp_id).parameters.push(parameter);
        }        
      }
      this.grandParameters = Array.from(mapGrandParameter.values());

      this.grandParameters.forEach((grandParameter: any) => {

        let totalpuntos = 0;
        let comments = "";
        
        grandParameter.parameters.forEach((parameter: any) => {
          totalpuntos = totalpuntos + parameter.qualtotal;
          if(parameter.comment && parameter.comment !== null){
            comments = comments + "<strong>" +parameter.name + ":</strong> " + parameter.comment + '<br><br>';
          }
        })
        grandParameter.totalpuntos = totalpuntos;
        grandParameter.comments = comments;
        
      });

      if (this.params.times.dateWithPlusMS) {
        this.verifyDate(this.params.times.dateWithPlusMS)
      }

      this.setHitZero()
    });
  }


  verifyDate(date: Number) {
    let now;
    let dateComp = date;
    setInterval(() => {
      now = new Date().getTime() / 1000;
      //console.log(now+'_'+dateComp)
      //console.log(( now >= dateComp))
      if (now >= dateComp) {
        this.showAceptButton = false;
      } else {

      }
    }, 1000);

  }

  openClaimGenerator(type: any, paramId: any) {
    let claimobj =
    {
      "eventid": this.paramsforsend.participation.event_id,
      "entryid": this.paramsforsend.participation.id,
      "competitor": this.paramsforsend.participation.participant_id,
      "judge": 0,
      "parametro": paramId,
      "typecase": type
    }

    this.stateChangeBar.emit(0);
    let self = this;
    setTimeout(function () {
      self.stateChangeBar.emit(14);
      self.claimObjet.emit(claimobj);
    }, 500)

  }


  aviabSending() {
    let ret = true;
    this.paramsforsend.judging.forEach((element: any) => {
      //console.log(element);
      if (ret) {

        if (!element.qualtotal) {
          if (element.qualtotal == 0 || element.qualtotal == '0') {

          } else {

            ret = false;
          }
        } else {

        }
      }

    });
    this.aviableSend = ret;
  }
  getParamsAndCriteriasforSend() {
    this.service.getGeneralJudgmentParams(this.entryid).subscribe((response: any) => {
      this.paramsforsend = response;
      let total = this.paramsforsend.generalTotal - this.paramsforsend.penaltyTotal
      this.total = (total > 0) ? total : 0;
      this.aviabSending()
      let self = this;
      setTimeout(function () {
        self.sendGeneralQuals()
      }, 500);

    });
  }
  setHitZero() {
    let hit = true;
    this.params.penalties.forEach((element: any) => {
      // console.log(element.judgements.length);
      if (hit == true) {
        if (element.judgements.length == 0) {
          hit = true
        } else {
          hit = false
        }
      }

      this.hitzero = hit;
    });

  }


  sendGeneralQuals() {
    this.paramsforsend.judge = 0;
    this.service.sendMain(this.paramsforsend).then((response: any) => {
      if (response) {
        Swal.fire({
          title: 'Success!',
          text: '',
          icon: 'success',
          showCancelButton: false,
          showConfirmButton: false
        });
        this.launchMainJudgeStatus.emit(1);
        this.getParamsAndCriterias();
        // this.eventFinishCreate.emit();
      }
    }).catch((error: any) => {
      if (error) {
        Swal.fire({
          title: 'Info!',
          text: 'Error on server',
          icon: 'info',
          showCancelButton: false,
          showConfirmButton: false
        });
      }
    });
    if (this.aviableSend) {
    } else {
      /* Swal.fire({
         title: 'Info!',
         text: 'Evaluate all Params',
         icon: 'info',
         showCancelButton: false,
         showConfirmButton: false
       });*/

    }

  }



  /******************************************************************************************/
  /******************************LO TOCO CHUCHO**********************************************/
  /******************************************************************************************/

  updateStateToAcceptNotes() {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result: any) => {
      if (result.isConfirmed) {
        this.service.athleteUpdateStateAcceptation(this.entryid)
          .then((response: any) => {
            Swal.fire({
              title: 'Info!',
              text: 'UPDATE SUCCESS',
              icon: 'success',
              showCancelButton: false,
              showConfirmButton: false
            });
          })
          .catch((error: any) => {
            Swal.fire({
              title: 'Info!',
              text: 'ERROR ON SERVER',
              icon: 'info',
              showCancelButton: false,
              showConfirmButton: false
            });
          });
      }
    });
  }
}
