import { Injectable } from '@angular/core';
import { BASEURI } from '../../shared/enviroment';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GrandParameterService {

  constructor(private Http: HttpClient) { }

  deleteGrandParameter(id: any) {
    return this.Http.delete(BASEURI + 'match/grandparameter/' + id).toPromise()
  }

  createGrandParameter(grandParameter: any) {
    let form = new FormData();
    form.append('grandParameter', JSON.stringify(grandParameter));
    if(grandParameter.id){
      return this.Http.post(BASEURI + 'match/grandparameter/' + grandParameter.id, form)
      .toPromise();
    }else{
      return this.Http.post(BASEURI + 'match/grandparameter/add', form)
      .toPromise();
    }
    
  }

  public updateGrandParameterOrdering(grandParameterOrdering: { grandParameterId: any, ordering: number}[]){
    let form = new FormData();
    form.append('grandParameterOrdering', JSON.stringify(grandParameterOrdering));
    return this.Http.post(BASEURI + 'match/grandparameter/ordering', form).toPromise();
  }

  public getParameters(): Observable<any> {
    return this.Http.get<any>(BASEURI + 'match/get/param').pipe(catchError(this.handelError));
  }

  public getGrandParameters(): Observable<any> {
    return this.Http.get<any>(BASEURI + 'match/grandparameter/list').pipe(catchError(this.handelError));
  }

  handelError(error: any) {
    return throwError(error.message || "Server Error");
  }
}
