import { Component, Output, Input, EventEmitter } from '@angular/core';
import { GrandParameterService } from '../grand-parameter.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-grand-parameter-list',
  templateUrl: './grand-parameter-list.component.html',
  styleUrls: ['./grand-parameter-list.component.scss']
})
export class GrandParameterListComponent {

  @Input() changes: any = 0;
  @Output() stateChange = new EventEmitter<any>();
  @Output() stateChangeBar = new EventEmitter<any>();
  @Output() currentEdit = new EventEmitter<any>();

  constructor(private grandParameterService: GrandParameterService) { }

  grandParameters: Array<any> = [];

  searchTerm: string = '';

  ngOnChanges() {

    if (this.changes == 1) {
      this.getGrandParameters();
      this.stateChange.emit(0);
    }

  }

  edit(edit: any) {
    console.log("edit");
    this.stateChangeBar.emit(18);
    let grandParameter = Object.assign({}, edit);
    this.currentEdit.emit(grandParameter);
  }

  add() {
    this.stateChangeBar.emit(17);
  }

  getGrandParameters() {
    this.grandParameterService
      .getGrandParameters()
      .subscribe((response: any) => {
        if (response) {
          this.grandParameters = response.Data;
        }
      }, (error: any) => {
        if (error) {
          console.log(error);
        }
      }
      );
  }

  delete(id: number) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.grandParameterService.deleteGrandParameter(id).then(() => {
          Swal.fire({
            title: 'Success!',
            text: 'GrandCategory deleted',
            icon: 'success',
            showCancelButton: false,
            showConfirmButton: false
          });
          this.getGrandParameters();
        }).catch((error: any) => {
          Swal.fire({
            title: 'Info!',
            text: 'Error on server',
            icon: 'info',
            showCancelButton: false,
            showConfirmButton: false
          });
        });
      }
    });
  }

  public incrementIndex(fromIndex: number) {
    var element = this.grandParameters[fromIndex];
    this.grandParameters.splice(fromIndex, 1);
    this.grandParameters.splice((fromIndex + 1), 0, element);

    this.updateOrdering();
  }

  public decrementIndex(fromIndex: number) {
    var element = this.grandParameters[fromIndex];
    this.grandParameters.splice(fromIndex, 1);
    this.grandParameters.splice((fromIndex - 1), 0, element);

    this.updateOrdering();
  }

  private updateOrdering() {
    let gramParameterOrdering = this.grandParameters.map((grandParameter, index) => {
      return { grandParameterId: grandParameter.id, ordering: index };
    });

    this.grandParameterService.updateGrandParameterOrdering(gramParameterOrdering).then();
  }
}
