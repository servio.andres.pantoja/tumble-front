import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { GrandParameterService } from '../grand-parameter.service';
import Swal from 'sweetalert2';

declare var UIkit: any;
@Component({
  selector: 'app-grand-parameter-creator',
  templateUrl: './grand-parameter-creator.component.html',
  styleUrls: ['./grand-parameter-creator.component.scss']
})
export class GrandParameterCreatorComponent implements OnInit {

  @Input() grandParameter: any = { parametersIds: [], parameters: [] };

  parameters: Array<any> = [];

  @Output() eventFinishCreate = new EventEmitter<any>();

  constructor(private grandParameterService: GrandParameterService) { }

  ngOnInit(): void {
    this.getParameters();
  }

  getParameters() {
    this.grandParameterService.getParameters().toPromise().then((response: any) => {
      this.parameters = response.Data
    }, (error: any) => {

    })
  }

  cancel() {
    this.eventFinishCreate.emit()
  }

  save() {

    this.grandParameter.parametersIds = (this.grandParameter.parametersIds) ? this.grandParameter.parametersIds : [];
    this.grandParameter.parameters = (this.grandParameter.parameters) ? this.grandParameter.parameters : [];

    if (this.grandParameter.title != '') {
      this.grandParameterService
        .createGrandParameter(this.grandParameter)
        .then((response: any) => {
          if (response) {
            Swal.fire({
              title: 'Success!',
              text: '',
              icon: 'success',
              showCancelButton: false,
              showConfirmButton: false
            });
            this.eventFinishCreate.emit();
          }
        }).catch((error: any) => {
          if (error) {
            Swal.fire({
              title: 'Error!',
              text: 'Error on server',
              icon: 'error',
              showCancelButton: false,
              showConfirmButton: false
            });
          }
        });
    } else {
      Swal.fire({
        title: 'Info!',
        text: 'Title and description required',
        icon: 'info',
        showCancelButton: false,
        showConfirmButton: false
      });

    }
  }

  onCheckParameter(element: any) {
    if (element.target.checked) {
      let parameter = this.parameters.filter(parameter => parameter.id == parseInt(element.target.value))[0];
      parameter.ordering = 0;
      this.grandParameter.parameters.push(parameter);
      this.grandParameter.parametersIds.push(parseInt(element.target.value));
    } else {
      let index = this.grandParameter.parametersIds.findIndex((rank: number) => rank === parseInt(element.target.value));
      if (index >= 0) {
        this.grandParameter.parametersIds.splice(index, 1);
        this.grandParameter.parameters.splice(index, 1);
      }
    }
    this.grandParameter.parameters.forEach((parameter: { ordering: number; }, index: number) => {
      parameter.ordering = index;
    });
  }

  public incrementIndex(fromIndex: number) {
    var element = this.grandParameter.parameters[fromIndex];
    this.grandParameter.parameters.splice(fromIndex, 1);
    this.grandParameter.parameters.splice((fromIndex + 1), 0, element);

    this.grandParameter.parameters.forEach((parameter: { ordering: number; }, index: number) => {
      parameter.ordering = index;
    });
  }

  public decrementIndex(fromIndex: number) {
    var element = this.grandParameter.parameters[fromIndex];
    this.grandParameter.parameters.splice(fromIndex, 1);
    this.grandParameter.parameters.splice((fromIndex - 1), 0, element);

    this.grandParameter.parameters.forEach((parameter: { ordering: number; }, index: number) => {
      parameter.ordering = index;
    });
  }
}
